/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Formation
 */
public class Tri {
    
    public static int [] superSort(int[] arrayToSort) {
       
        
        
        boolean sorted = false;
        while( !sorted) {
             sorted = true;
             
             for(int i = 0 ; i < arrayToSort.length -1; i++) {
                 
             if (arrayToSort[i] > arrayToSort[i+1]) {
                var tmp = arrayToSort[i];
                arrayToSort[i] = arrayToSort[i+1];
                arrayToSort[i+1] = tmp;
                sorted =false;
            }
        }
        
        
        
        
      }
      return arrayToSort;
    }
}
        